#!/usr/bin/env bash
uploadfolder = "$1"
if [[ "$uploadfolder" == "" ]]; then 
    echo "Need  uploadfolder for migration"
    echo "Look into lib/conifg.local.php for info"
    exit 1
fi
if [[ ! -d $uploadfolder ]]; then
    echo "Uploadfolder $uploadfolder does not exist."
    exit 1
fi
mkdir $uploadfolder/local
echo "migration done"